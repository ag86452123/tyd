# TheYaddo Website
TheYaddo is a free-thinking community that compels women to find freedom in the self. Through a constant stream of evergreen content to read, listen, and watch, women are helped to find confidence and beauty in everything around them using the beauty from within them.

## Our Mission
Our mission is to help women find freedom by arming them with information and products that promote body positivity, sexual liberation, strength and growth.

## Meta
### Main  Headline
Find freedom in your self

### Main Description
TheYaddo is where young women come to find freedom in self. Read, listen, and watch evergreen content for body positivity, sexual liberation, strength and growth.