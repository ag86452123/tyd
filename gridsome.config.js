//
    // Configuration File
    // @package tyd

require('yamlify/register')

const
    path = require('path'),
    data = require('./content/sitemeta.yml'),
    siteName = data.site.name,
    siteDescription = data.site.description,
    siteDomain = data.site.domain,
    siteUrl = data.site.domain,
    siteTheme = data.brand.primary_color,
    gId = data.google_analytics.id,
    slugify = require('@sindresorhus/slugify')
    addStyleResource = (rule) => {
        rule
            .use('style-resource')
            .loader('style-resources-loader')
            .options({
                patterns: [
                    path.resolve(__dirname, './src/assets/css/config.scss'),
                    path.resolve(__dirname, './src/assets/css/mixins.scss')
                ],
            })
    }

module.exports = {
    siteName,
    siteDomain,
    siteUrl,
    siteDescription,
    siteTheme,
    titleTemplate: `${siteName} - %s`,
    permalinks: { trailingSlash: false },
    chainWebpack(config) {
        const types = ['vue-modules', 'vue', 'normal-modules', 'normal']
        types.forEach(type => addStyleResource(config.module.rule('scss').oneOf(type)))
    },
    plugins: [
        {
            use: '@gridsome/plugin-sitemap'
        },
        {
            use: 'gridsome-plugin-robots',
            options: {
                policy: [{ userAgent: '*', allow: '/' }]
            }
        },
        {
            use: '@gridsome/plugin-google-analytics',
            options: {
                id: gId
            }
        },
        {
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'CustomPage',
                path: './content/pages/**/*.md'
            }
        },
        {
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'Author',
                path: 'content/authors/**/*.md'
            }
        },
        {
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'Topic',
                path: 'content/topics/**/*.md'
            }
        },
        {
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'More',
                path: 'content/morelinks.yml'
            }
        },
        {
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'Social',
                path: 'content/social.yml'
            }
        },
        {
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'Story',
                path: './content/stories/**/*.md',
                refs: {
                    author: 'Author',
                    topic: 'Topic',
                    type: {
                        typeName: 'Type',
                        create: true
                    }
                }
            }
        }
    ],
    transformers: {
        remark: {
            imageQuality: 100,
            plugins: [
                ['@noxify/gridsome-remark-classes', {
                    'paragraph': 'r-is-p',
                    'heading': 'r-is-h'
                }],
                [ '@noxify/gridsome-plugin-remark-embed', {
                    'enabledProviders': ['Youtube', 'Giphy', 'Vimeo', 'Twitter'],
                    'YouTube': {
                    },
                    'Giphy': {
                        responsive: true
                    },
                    'Vimeo': {
                        responsive: true
                    },
                    'Twitter': {
                        align: 'center',
                        linkColor: '#ff0f7b',
                        hideMedia: false
                    }
                }]
            ]
        }
    },
    templates: {
        Story: [{
            path: node => `/${slugify(node.title)}`
        }],
        Topic: [{
            path: node => `/${node.id}`,
            component: '~/templates/Topic.vue'
        }],
        Author: [{
            path: node => `/${node.id}`,
            component: '~/templates/Author.vue'
        }],
        Type: [{
            path: node => `/${slugify(node.title)}`,
            component: '~/templates/Type.vue'
        }]
    }
}