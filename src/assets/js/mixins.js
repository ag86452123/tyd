//
    // Custom (and Helper) Functions
    // @package tyd

export default {
    install(Vue) {
        Vue.mixin({
            methods: {
                // Pull images out of p tags
                unwrapImages(images = Array.from(document.querySelectorAll('p img'))) {
                    if (!images) return;
                    images.forEach(image => {
                        const pos = image.parentNode
                        pos.insertAdjacentElement('afterend', image)
                        pos.remove()
                    })
                }
            }
        })
    }
}