'use strict'

import Vuex from 'vuex'
import store from '../store/store'
import VueStringFilter from 'vue-string-filter'
import vsmMenu from 'vue-stripe-menu'
import InfiniteLoading from 'vue-infinite-loading'

import Mixins from '~/assets/js/mixins'

import Sprite from '~/components/tools/Sprite'
import Default from '~/components/layouts/Default'
import FullWidth from '~/components/layouts/FullWidth'
import Container from '~/components/layouts/Container'
import HomeLink from '~/components/links/HomeLink'
import Newsletter from '~/components/links/Newsletter'
import Social from '~/components/links/Social'

import 'vue-stripe-menu/dist/vue-stripe-menu.css'
import '~/main.scss'

export default function (Vue, { appOptions, head }) {
    Vue.use(Vuex)
    Vue.use(VueStringFilter)
    Vue.use(Mixins)
    Vue.use(vsmMenu)
    Vue.use(InfiniteLoading)

    Vue.component('r-sprite', Sprite)
    Vue.component('r-default', Default)
    Vue.component('r-home-link', HomeLink)
    Vue.component('r-fullwidth', FullWidth)
    Vue.component('r-container', Container)
    Vue.component('r-newsletter', Newsletter)
    Vue.component('r-social', Social)

    appOptions.store = store

    head.link.push({
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=DM+Serif+Display|Playfair+Display:400,700&display=swap'
    })
    head.script.push({
        src: 'https://player.vimeo.com/api/player.js',
        body: true
    })
}