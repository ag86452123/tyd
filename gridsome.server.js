require('yamlify/register')

const
    path = require('path'),
    optionsObject = {},
    data = require('./content/sitemeta.yml'),
    siteDomain = data.site.domain,
    siteImage = path.resolve(__dirname, data.default_image.replace('../../', './')), // Enable Gridsome locate image and add it to store.
    siteImageDesc = data.default_image_description,
    siteTheme = data.brand.primary_color,
    siteThemeDark = data.brand.theme_dark,
    siteThemeBlack = data.brand.theme_black

module.exports = function(api) {
    const templates = new Map([
        ['home', './src/templates/Home.vue'],
        ['about', './src/templates/About.vue'],
        ['default', './src/templates/Default.vue'],
        ['404', './src/templates/404.vue']
    ])
    api.loadSource(async store => {
        store.addMetadata('siteDomain', siteDomain),
        store.addMetadata('siteImage', siteImage),
        store.addMetadata('siteImageDesc', siteImageDesc),
        store.addMetadata('siteTheme', siteTheme),
        store.addMetadata('themeDark', siteThemeDark),
        store.addMetadata('themeBlack', siteThemeBlack)
    })
    api.createPages(async ({graphql, createPage}) => {
        const { data } = await graphql(`{
            allCustomPage {
                edges { node { id, template, slug } }
            }
        }`)
        data.allCustomPage.edges.forEach(({ node }) => {
            const component = templates.get(node.template) || './src/templates/Default.vue'
            createPage({
                path: `/${node.slug}`,
                component,
                context: { id: node.id }
            })
        })
    })
    api.onCreateNode(options => {
        if (options.internal.typeName === 'Story' && !options.publish) return null
        if (options.internal.typeName === 'Author' || options.internal.typeName === 'Topic') optionsObject[options.fileInfo.path] = options.id
        if (options.internal.typeName === 'Story') {
            options.author = options.author.map(option => optionsObject[option])
            options.topic = options.topic.map(option => optionsObject[option])
            return {
                ...options
            }
        }
    })
}