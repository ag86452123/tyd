---
publish: true
excerpt: how to self care without breaking the bank
image: "../../src/assets/images/nick-owuor-astro-nic-visuals-xow-tdt1wfu-unsplash.jpg"
image_description: A young lady
created_at: 2020-06-12T13:58:52.000+00:00
type: Read
pastel: Ivory
title: 10 Ways to Self Care for the Broke Girl
author:
- content/authors/ms-sygala.md
topic:
- content/topics/beauty.md

---
Life can come at you fast and having a self care routine is a necessary method of survival.

Family, work, responsibilities, relationships, demands, money… Somebody say HEC TIC!

Think about it this way, you keep the light switch on for days without turning it off. After some time it starts to dim, and then flicker before it eventually burns out.

Thats exactly what happens to us (as humans) when we overdo it, trying to evolve, be better - in work, life, studies and relationships. This can take a toll on our mind, body and soul, often leading to lethargy and burn out.

Even in planning a simple vacation, some of us (and by us I mostly mean me) like to plan extravagantly and when it can’t be the way we want it, we postpone.

When I think of self care, my mind goes to a beach in Honolulu with endless books and mai tai's surrounded by expert yogis learning about chakras and chakrasanas...

[https://giphy.com/gifs/26CaMv4RXGpYZzsS4](https://giphy.com/gifs/26CaMv4RXGpYZzsS4 "https://giphy.com/gifs/26CaMv4RXGpYZzsS4")

But life has a rude way of yelling STOP! Reality check!

So, this article is for the days when you feel too tired but can’t make a trip or splurge on an expensive spa. Sometimes, as the name clearly suggests, self care starts with the love you have for yourself.

Below are 10 self care regimens that would cost you almost nothing but are super effective in making you feel better about yourself

### **1. Meditate**:

This always seemed so cliché to me. Meditate, yoga and kumbaya.

But it's not even that deep. Just setting a few minutes aside every morning to bless your day, breathe deeply, close your eyes and speak positivity into your life can go a long way. Listen to some calming music or rain sounds, whatever that is soothing to you would definitely have you feeling relaxed and lighter.

Click the link below for my gift to you. 15 minutes of calming meditation music

[https://youtu.be/aIIEI33EUqI](https://youtu.be/aIIEI33EUqI "15 mins meditation music")

### 2. M**ake breakfast a big deal**

It doesn't have to be every day and it doesn't have to be anything super complicated, (you're not on masterchef sis!)

It could be on Saturday mornings when you don’t have to dash out to work. Go on YouTube and pick a simple recipe to make for yourself. Bring out your best china, play some music and hum along as you cook a meal for you. Settle down and eat with gusto. You deserve it!

![full tray of easy breakfast to make for yourself. Frenchtoast and syrup](../../src/assets/images/randy-fath-sq20twzxxo0-unsplash.jpg)

### 3. **Unplug from the outside world**.

Yeah, you need to put your phone away for this one. Find joy in solitude. Read a book, something easy, nice, funny or romantic. It is cool to read self help books and motivational mumbo jumbo, but sometimes, what your brain requires to loosen up is so much simpler...

Or if you're not a reader, you can also binge watch romantic feel good movies. **Pretty Woman** is always top on my list!

![a young girl reading a book to help her relax](../../src/assets/images/demorris-byrd-ez98bl75dei-unsplash.jpg)

### 4. Take **care of your skin**.

Best way to show love to yourself is by taking care of the skin you're in. Remember that face scrub you bought eons ago? Be sure it isn’t expired and slather it on. Or better still, DIY - mash one half of a banana in a bowl, mix in a tablespoon of orange juice and a tablespoon of honey, apply mask to face for 15 mins. Wash your hair, exfoliate your skin and relax.

[https://giphy.com/gifs/oz9y20nb1b0XgFRqAa](https://giphy.com/gifs/oz9y20nb1b0XgFRqAa "https://giphy.com/gifs/oz9y20nb1b0XgFRqAa")

### 5. **Listen to some music.**

Music is a drug. And that's on period!

Create a playlist of your favourite songs, from the 90s, 80s, Y2k, whatever. A nice blend of music that reminds you of good times. Get a makeshift microphone and bring out your inner beyonce. Dance like no one is watching (cause they aint), sing out to the high heavens and let your self loose!!!

[https://giphy.com/gifs/143qWPF33HtSTK](https://giphy.com/gifs/143qWPF33HtSTK "https://giphy.com/gifs/143qWPF33HtSTK")

### 6. **Gift Yourself.**

Well, duh! Spoil yourself; buy a nice dress, a new lipstick, set a spa appointment or go to a fancy restaurant and have a nice meal. I know the point of this article is to tell you how to self care without needing to spend money, but don’t you think you deserve a little reward for all the hard work and isolation?

### 7. **Do something creative**.

There is something particularly refreshing about creating something. It could be writing a short story, drawing, painting, sewing, moulding, sculpting… It doesn't matter and it doesn't have to be perfect. Just spending time and bringing something to life with your hands is more relaxing than you'll ever know

[https://giphy.com/gifs/dvNwhVCMyvEwSLPRem](https://giphy.com/gifs/dvNwhVCMyvEwSLPRem "https://giphy.com/gifs/dvNwhVCMyvEwSLPRem")

### **8. Go outside**

Step outside for a bit of fresh air everyday. Go for a short walk, a run or just lounge out in natures air. Either ways, being outside is amazing therapy and is something i enjoy doing early in the mornings when the birds are still chirping and the sun is not scorching.

> “Keep close to Nature’s heart… and break clear away, once in a while, and climb a mountain or spend a week in the woods. Wash your spirit clean.” –John Muir

[https://giphy.com/gifs/7YCCkD1uR1gZ2nsSJF](https://giphy.com/gifs/7YCCkD1uR1gZ2nsSJF "https://giphy.com/gifs/7YCCkD1uR1gZ2nsSJF")

### **9. Tea and Candles.**

After a long day, you want to settle down, light some sweet smelling candles unwind and relax with a cup of camomile or peppermint tea - whatever suits your fancy. I promise you would feel light as a feather when you shut the world out, relax and Sleep!

I personally like to to shop aromatherapy candles from [Abela by Scents of Africa](https://abelaworld.com/product-category/pure-essence-aromatherapy/).

### **10.Premium Orgasms.**

Ladiesss. Self care is also release. All that tension, stress and anxiety can unfurl in one big O. Super glad that this option doesn’t **always** require a human partner. So stock up on some nice vibrators. The wands are becoming super popular.

![a vibrating magic wand aka a sex toy](../../src/assets/images/nalone-super-powerful-multi-speed-waterproof-g-spot-av-wand-sex-toys-magic-wand-massager-vibrators.jpg)

And if you're scared of numbing your clitoris or like that lady on twitter who almost got her clitoris burnt, your fingers are also just perfect!

[https://giphy.com/gifs/l0HlUHWbjYgK211zG](https://giphy.com/gifs/l0HlUHWbjYgK211zG "https://giphy.com/gifs/l0HlUHWbjYgK211zG")