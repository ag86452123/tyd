---
publish: true
excerpt: Succinct summary of the Story. Keep it at max 155 characters. Read more here.
image: "../../src/assets/images/annie-spratt-n3t1gbygkjo-unsplash.jpg"
image_description: An empty bed. Emotionless relationship
created_at: 2020-06-05T14:38:00Z
type: Read
pastel: Floral
title: Avoiding The Path Of The Death Bed (An Lgbtq Flavored Article)
author:
- content/authors/ezinne-ogwumah.md
topic:
- content/topics/sex.md

---
**Avoiding The Path Of The Death Bed (An LGBTQ Flavoured Article)**

Like the common rainbow phrase "_coming out"_, take off this article whatever you will.

While some proudly fly the flag and some stay closeted, we can't deny the existence of sexual issues in same sex relationship.

This article is purely written from a personal and a lesbian point of view.

![](../../src/assets/images/pawel-czerwinski-3k9pgkwt7ik-unsplash.jpg)

Now, what is a death bed? I first came across this term while trying to improve sexual life with the very first girlfriend. For a first timer, it was quite shocking that as we _(ladies)_ were meant to understand our bodies and some touches weren't causing any form of electrical connections.

Imagine having to fake an orgasm when your partner is clearly aware. _Tragic_.

However, these are ways we were able to move past that:

**Communication**  
This can mean anything and everything from a grudge, unsettled emotions, a sudden turn off or the need for something new.  
Mine was as a result of a turn off. Now, I have a weird partner who appreciates everything but _cornrows_. Who, who would have thought _cornrows_ aren't hot?

**Hygiene**  
Nobody likes dirty and by dirty, I mean, keeping your temple _(body)_ clean. I caught a funny tweet off Twitter, about a lady who had a crush on another girl only to discuss she showers twice a week. Her "_crushy_" feelings went away with the wind.  
Now, supposing that revelation never happened till the point they were _in too deep_ with themselves, what do you think?

![](../../src/assets/images/sharon-mccutcheon-p9zpitllsbk-unsplash.jpg)

**Keep It Kinky**  
One day! Just one day, I'll speak about how much the LGBTQ community contributes to the kink community. You don't have to be an amateur to be a kink or pro "_Grey_". A little cuffs, blindfold and a bullet vibrator will do some magic.

Lastly, understanding. I think this still highlights communication because that is what it takes to get to the point of understanding.  
Get to know your partner's mood.  
Your partner's body.  
The play rules of the bed you share.

I hope these help! You can reach out if you have any other questions.![](../../src/assets/images/123-let-s-go-imaginary-text-704767.jpg)