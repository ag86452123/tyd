---
publish: true
excerpt: 10 men discuss their very hilarious wedding day regrets. Its not what you
  would expect
image: "../../src/assets/images/mariah-ashby-kp_3fvsxede-unsplash.jpg"
image_description: Married man knotting his tie
created_at: 2020-06-12T23:21:10.000+00:00
type: Read
pastel: Aliceblue
title: 10 Men Admit The Things They Regret The Day They Tied The Knot
author:
- content/authors/ms-sygala.md
- content/authors/adebayo-emuelemo.md
topic:
- content/topics/sex.md

---
Traditionally, wedding ceremonies appear to be more about the ecstatic bride while the groom sashays through the day’s proceedings with stoic involvement accompanied by beaming smiles and pleasantries for the elegantly dressed invited guests with their matching fabrics and the mogbo-moyas.

Marriage is a big deal and because of how big a deal it is, there are always a number of things you can screw up when tying the knot.

I spoke to a few guys and one thing is certain, no matter how lit the wedding was, the regrets always come the morning after ... along with the hangover

## **Letting Parents Take Over**

> I let my parents have too much influence. I'm an only child and my parents weren't going the traditional route of letting the brides family make most of the decisions. It seemed like my wife and I ended up attending my parents wedding. There were no boundaries and I regretted that so much.

* Demi 45, married 10 years

## Fighting

> I fought on my wedding day. Lol. I wish I hadn't lost my cool, but no one prepares you for the annoying things people do - like a random person picking up your money on the dance floor. No one knew the guy or where he had come from. But in hindsight, I should have let my best man or some one else handle it instead of decking him myself.

* Alex 29, married 2 years

  ## Getting Shit-faced Drunk

> I shouldn't have drank as much as I did. I cant even remember the wedding. People were just giving me drinks, nobody gave me food. I danced so much I forgot it was my wedding thought i was back in quilox. Last i remember was throwing my lungs up and going to rest in the car. I woke up a married man but to a very angry wife. 
>
> Wedding pictures were a mess because my eyes were so unfocused

* Tobe 38, married 5 years

## Inviting my Ex

> I invited my ex and that was an epic fail. I mean we were cool but for some reason it was just awkward seeing her there. Awkward for me, awkward for her, awkward for my wife, my friends, every one. 
>
> She left and we haven't spoken since them... Just as well I guess![A man covering his face out of shame because he invited his ex to his wedding. Awkward](../../src/assets/images/monochrome-photo-of-a-man-hiding-his-face-2954393.jpg)

* Anonymous 35, married 8 years

## No Personal vows

> I have been married so long, the only thing I think I regret is not making   our vows more personal. But we had an anniversary and we fixed that

* Lere 48, married 20 years

## Too Stoned

> That was the biggest day of my life I swear! The day i became a man MAN  but I was so high I don’t remember so many things. 

* Dare 36, married 6 years

![whatsapp conversation about what dare regrets about his wedding day. Too stoned to remember ](../../src/assets/images/greyscale-photo-social-media-day-social-media-graphic.png)

## Should have hired a planner

> The sitting arrangement was a mess and I just wished everyone could have gotten food equally.  

* Lawson 32, married 2 years

![whatsapp conversation about what lawson regrets about his wedding day. no event planner ](../../src/assets/images/greyscale-photo-social-media-day-social-media-graphic-2.png)

## Bach Eve Should NOT have been the night before

> I had a hangover from my bachelor party and had a terrible headache throughout my engagement in the morning. 
>
> Pro tip: Never have your bach eve the night before your wedding

* IK 35, married 3 years

## Should Have Been Strictly By Invitation 

> I regret that we didn’t make our wedding strictly by invitation. We budgeted for 400-500 people but the crowd was mad that day. Over 1000 people. Sadly, nothing was enough.

Jordan 30, married 2 years

![A guestlist would have made Jordans wedding a lot smaller and simpler](../../src/assets/images/markus-winkler-tgqs5wjuzjk-unsplash.jpg)

## My Shoes!!!

> Wearing new shoes on your wedding day is a huge risk. I was so uncomfortable, it was a nightmare. 

* Wande, 31 (Married for 2 years)

  ![wearing comfortable shoes for your wedding will save a lot of stress](../../src/assets/images/shelbey-miller-hiqd4db8kr8-unsplash.jpg)

In conclusion, my advice to every couple to be is wear shoes that are comfortable not flashy, lay off the booze, use an event planner and actually have FUN!