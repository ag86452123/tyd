---
image: "../../src/assets/images/000064760008.jpg"
created_at: 2020-05-04T11:45:05.000+00:00
type: Watch
pastel: Milkthistle
title: 12 Places to Find Fat- and Body-Positive Workouts You Can Do at Home
author:
- content/authors/anastasia-burrito.md
- content/authors/chioma-ezeibe.md
- content/authors/sarah-jacoby.md
topic:
- content/topics/fitness.md
- content/topics/food-drink.md
excerpt: Elevate your technical SEO game with clear, actionable ideas to improve your
  website's search presence.
image_description: Fat and women working out happy and proud of her shape or dare
  I say "wideness".
publish: false

---
So here we are, about a month or more (depending on where you live) into [stay-at-home](https://www.self.com/story/social-distancing-quarantine-isolation) orders in response to the [coronavirus pandemic](https://www.self.com/health-conditions/covid-19-coronavirus). Some days I feel like I’m good, and other days I feel like I am barely hanging on! Maybe you can relate? It’s been a stressful time trying to navigate life during a pandemic. A lorem ipsum for purposes of...uh, let's see

I personally rely heavily on exercise for [stress management](https://www.self.com/story/pandemic-therapy-trends). Movement helps me get through difficult times.

## This is a heading level 2

So here we are, about a month or more (depending on where you live) into [stay-at-home](https://www.self.com/story/social-distancing-quarantine-isolation) orders in response to the [coronavirus pandemic](https://www.self.com/health-conditions/covid-19-coronavirus). Some days I feel like I’m good, and other days I feel like I am barely hanging on! Maybe you can relate? It’s been a stressful time trying to navigate life during a pandemic.

> When stress in my life feels like a dark storm, a good workout helps me feel like I’m on the other side of it
>
> _-- Michelle Obama_

In response to the numbers of people who are trying to figure out how to [work out at home](https://www.self.com/story/exercise-at-home), tons of instructors have come out of the woodwork to help others move their bodies and adapt to the new situation. It’s just another way I see the amazing beauty of humanity through this situation!

## A YouTube Video

To embed a video, all you need to do is paste in the sharing link wherever you wanna see the video and the magic of system will unwrap it as a playable video. Sweet, huh?

[https://youtu.be/3MX7ByAbVyw](https://youtu.be/3MX7ByAbVyw "https://youtu.be/3MX7ByAbVyw")

[So here we are](https://youtu.be/3MX7ByAbVyw), about a month or more (depending on where you live) into [stay-at-home](https://www.self.com/story/social-distancing-quarantine-isolation) orders in response to the [coronavirus pandemic](https://www.self.com/health-conditions/covid-19-coronavirus). Some days I feel like I’m good, and other days I feel like I am barely hanging on!

Maybe you can relate? It’s been a stressful time trying to navigate life during a pandemic.

## A Giphy Embed

Just like with YouTube, you just need to paste the short link.

[https://giphy.com/gifs/wWqirevIlombK](https://giphy.com/gifs/wWqirevIlombK "https://giphy.com/gifs/wWqirevIlombK")

## A Twitter Embed

Even to embed Twitter, all you need is to copy and paste the link like below. **Note: copy and paste the link, not the embed code from Twitter.**

[https://twitter.com/seventeen/status/1261048391070625792](https://twitter.com/seventeen/status/1261048391070625792 "https://twitter.com/seventeen/status/1261048391070625792")

But because people of all body types are likely feeling stressed and looking for workout options, it’s important that we shine a light on some size-inclusive options for folks who may need it.

## A Video from Vimeo

To embed a video, all you need to do is paste in the sharing link wherever you wanna see the video and the magic of system will unwrap it as a playable video. Sweet, huh?

[https://vimeo.com/239029778](https://vimeo.com/239029778 "https://vimeo.com/239029778")

In response to the numbers of people who are trying to figure out how to [work out at home](https://www.self.com/story/exercise-at-home), tons of instructors have come out of the woodwork to help others move their bodies and adapt to the new situation. It’s just another way I see the amazing beauty of humanity through this situation!

## Another heading level 2

In response to the numbers of people who are trying to figure out how to [work out at home](https://www.self.com/story/exercise-at-home), tons of instructors have come out of the woodwork to help others move their bodies and adapt to the new situation. It’s just another way I see the amazing beauty of humanity through this situation!

![Lip glosses from me.](../../src/assets/images/product-roundup_glow.jpg "Some real sweet lip gloss")

Others might be more motivated by someone who resembles their body type producing movement that is attainable without diet-culture syntax.  Yet others might be more motivated by someone who resembles their body type producing movement that is attainable without diet-culture syntax.

Others might be more motivated by someone who resembles their body type producing movement that is attainable without diet-culture syntax.  Yet others might be more motivated by someone who resembles their body type producing movement that is attainable without diet-culture syntax.

![](../../src/assets/images/the_yaddo_default_image.jpg)

### Heading level 3?

So here we are, about a month or more (depending on where you live) into [stay-at-home](https://www.self.com/story/social-distancing-quarantine-isolation) orders in response to the [coronavirus pandemic](https://www.self.com/health-conditions/covid-19-coronavirus). Some days I feel like I’m good, and other days I feel like I am barely hanging on! Maybe you can relate?

### Another h3

Where the sky is clear and the water is calmer. Lately though, relying on workouts has come with some obstacles. Gyms and recreation centers are closed, and finding space to [work out safely outside](https://www.self.com/story/is-exercise-outside-safe-coronavirus) can be tricky.

* Some people appreciate the specialized
* Approach of size-inclusive fitness so they can have
* Options to work out sitting down or follow an instructor who understands how larger bodies experience movement
* And the potential spatial differences with respect to range of motion.

Others might be more motivated by someone who resembles their body type producing movement that is attainable without diet-culture syntax.

Some days I feel like I’m good, and other days I feel like I am barely hanging on! Maybe you can relate?

1. If you are looking for fitness at home that represents
2. Size diversity, works for larger bodies, and
3. Celebrates size inclusivity across the board, here are 12 of my favorite options out there.

Get to know these beautiful people and their amazing services, all currently available online!

![](../../src/assets/images/s-03.jpg)

So here we are, about a month or more (depending on where you live) into [stay-at-home](https://www.self.com/story/social-distancing-quarantine-isolation) orders in response to the [coronavirus pandemic](https://www.self.com/health-conditions/covid-19-coronavirus). Some days I feel like I’m good, and other days I feel like I am barely hanging on! Maybe you can relate? It’s been a stressful time trying to navigate life during a pandemic. I personally rely heavily on exercise for [stress management](https://www.self.com/story/pandemic-therapy-trends). Movement helps me get through difficult times.

> So here we are, about a month or more (depending on where you live) into

Where the sky is clear and the water is calmer. Lately though, relying on workouts has come with some obstacles. Gyms and recreation centers are closed, and finding space to [work out safely outside](https://www.self.com/story/is-exercise-outside-safe-coronavirus) can be tricky.

**Cost:** Free