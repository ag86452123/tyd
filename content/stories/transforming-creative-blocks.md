---
publish: true
excerpt: 'Creatives tend to get frozen most of the time for one reason or the other.
  This article teaches you how to transform the blocks back to creativity '
image: "../../src/assets/images/crumpled-sheet-paper.jpg"
image_description: 'crumpled sheet of paper '
created_at: 2020-06-05T13:29:38Z
type: Read
pastel: Blush
title: 5 Ways to Transform Creative Blocks
author:
- content/authors/ezinne-ogwumah.md
topic:
- content/topics/work-money.md

---
**Transforming Creative Blocks**

A creative block isn't necessarily what only artists experience. A creative block is the inability to draw inspiration from within or outside.  
There are a couple of things that can be responsible for creative blocks and the very first step of transforming the situation is first being aware of the cause.

One of the common reasons for creative blocks are anxiety, which could be about an upcoming project or the need for perfection. As prevalent as this is, it could be one of the hardest, as it has to do more with the mind.  
Well, the fastest way to overcome anxiety is to question and see through the object of worry.  
"_What if things don't go as plan, what are the alternatives?"_ or "_What if there aren't any alternatives, what is the way forward?"_

1. **Imbibing a lifestyle of routine** can help with managing creative blocks. With a systematic lifestyle, comes commitment and that is before I chip in the idea of meditation. Meditations are routines and can't be adhered to without commitment. So, how committed are we to the thing we are struggling to create, connect and share?![](../../src/assets/images/yellow-clock.jpeg)
2. Another way is **finding what works for you**. This took a lot of time for me to realize. As a writer, I had consistently looked at the creation process of my peers and found myself struggling.  
   Understand that everyone is different in every single way and know peace.  
   I write better on my phone's notepad than my laptop. Some people create better by hand writing and so on. So, it could be that the limitations you are facing, are you trying to conform to a nonexistent norm.
3. Financial and personal issues can also be a huge block but here's the trick, ignorance doesn't help in resolving issues. You have to face whatever struggle or distractions it would bring.  
   Which brings me to say that **whatever constraints you are experiencing, there's absolutely a way around it__ _even with no finances_ **Yes**! _The Obstacle is the only way - Ryan Holiday_

   ![](../../src/assets/images/financial-freedom.jpeg)  
   The interesting thing about creativity is the ability to **adapt and innovate** because it doesn't matter what, who or where you are, as long as there is consistency, things eventually come around.
4. And then finally, over commitment. I particularly suffer from this. A problem of "_Yes_" and an issue of "_staying relevant"._ **You can't achieve anything while trying to do many things**, so before you affirm or commit to a certain thing, sign up for an activity that you're aware you might not be up for, **evaluate your time and mental expectations** for work. Easier said than done but achievable when aware.
5. For an extra touch - criticism and rejection. Lately, I have been seeing a few colleagues lament about rejection mails or backlashes from critics and the "_opinionated_" ones. Personally, I would say I have had a minute share of criticism and before I let self-doubt consume me and mentally block my creative energy, I picked out what I could learn **there are sometimes, advice in opinions** and embraced the fact that, **I can't please everyone.  
   I mean, nobody can or can you?**

![](https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif)