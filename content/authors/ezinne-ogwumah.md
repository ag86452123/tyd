---
title: Ezinne Ogwumah
id: theyaddo.com/ezinne
avatar: "../../src/assets/images/avater2-1.jpg"
role: Asst. Editor
short_bio: Physically, a big Igbo girl. Mentally, a living Paradox
social_links:
  link:
  - title: instagram
    full_url: 'https://instagram.com/ezinneogwumah '
  - title: twitter
    full_url: https://twitter.com/ezinneogwumah

---
