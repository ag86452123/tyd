---
title: GAS
avatar: "../../src/assets/images/untitled-design-5.png"
id: gas
role: Senior Editor
short_bio: Patia Braithwaite is a writer and editor who joined SELF in May 2019. She
  was previously the wellness editor at.
social_links:
  link:
  - title: Twitter
    full_url: https://twitter.com/msssygala

---
Sarah Jacoby is a health and science journalist and is especially interested in the science of skin care, sexual and reproductive health, drugs and drug policy, mental health, and helping everyone find their personal definition of wellness. She's a graduate of NYU's Science, Health, and Environmental Reporting Program and has a background in psychology and neuroscience.