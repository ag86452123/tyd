---
title: Anastasia Burrito
id: anastas
avatar: "../../src/assets/images/amy-marturana-headshot.jpg"
role: Senior Health Editor
short_bio: "Gigi Engle is a feminist writer, certified sex coach, and sex educator.
  As a sex educator with the Alexander."
social_links:
  link:
  - title: Facebook
    full_url: https://www.facebook.com/GigiEngleWriter
  - title: Instagram
    full_url: https://www.instagram.com/gigiengle
  - title: Twitter
    full_url: https://www.twitter.com/gigiengle

---
Last year, the design gods decided that dark modes were the new hotness. "Light colors are for suckers", they laughed, drinking matcha tea on their fixie bikes or whatever.