---
title: Adebayo Emuelemo
type: Read
id: theyaddo.com/adebayoemuelemo
avatar: "../../src/assets/images/avater1.jpg"
role: Contributor
short_bio: A regular guy on a good day and a sage on better days; Tbh, I'm just a
  serial hedonist
social_links:
  link:
  - title: instagram
    full_url: https://instagram.com/oluwafemisossa
  - title: twitter
    full_url: https://twitter.com/oluwafemisossa

---
