---
title: Venus Astralis
id: venus
avatar: "../../src/assets/images/download.jpg"
profile_picture: ''
role: Contributor
short_bio: "Sarah Jacoby is a health and science journalist and is especially interested
  in the science of skin care, sexual."
social_links:
  link:
  - title: Instagram
    full_url: https://instagram.com
  - title: Twitter
    full_url: https://twitter.com

---
There’s lots of tutorials on [how to build dark modes](https://css-tricks.com/dark-modes-with-css/) already, but why limit yourself to light and dark? Only a Sith deals in absolutes.

That’s why I decided to build a new feature on my site:
**dynamic color themes!** Yes, instead of two color schemes, I now have ten! That’s eight better than the previous website!