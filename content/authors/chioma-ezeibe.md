---
title: Chioma
id: chioma
avatar: "../../src/assets/images/011618_SELF_2903.jpg"
role: Freelance Contributor
short_bio: "Tiffany Dodson is the Associate Market Editor at SELF, starting with
  the brand in early 2017. Tiffany speciali."
social_links:
  link:
  - title: Instagram
    full_url: https://www.instagram.com/tiffdods

---
This makes it challenging to manage everything as we **update** our APIs, and it exposes some of the drawbacks of a RESTful Architecture. **GraphQL** solves this problem. Today, we're going to show you how to **write** **GraphQL** Apps using AWS Lambda. We'll start with an overview of **GraphQL** and why it's better than the normal REST Architecture.