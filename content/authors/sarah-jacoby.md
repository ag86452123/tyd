---
title: Sarah Jacoby
id: sarah
avatar: "../../src/assets/images/jessica-cruel.jpg"
role: Contributor
short_bio: "A Southern belle trying to find beauty in the Big City. Collects candles—but
  never burns them—and has a fridge."
social_links:
  link:
  - title: Twitter
    full_url: https://twitter.com/pdotbrathw8
  - title: Website
    full_url: https://k16e.co/kb

---
