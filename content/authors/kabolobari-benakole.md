---
title: Kabolobari Benakole
avatar: "../../src/assets/images/iwd_2020_each_for_equal_pose_02.jpg"
id: kb
role: Senior Staff Writer
short_bio: Shauna is a self-proclaimed nerd, hip-hop head, jock, and yogi. A graduate
  of Stanford, UCLA and Johns Hopkins.
social_links:
  link:
  - title: Website
    full_url: https://k16e.co/kb
  - title: Twitter
    full_url: https://twitter.com/kabolobari

---
Amy is a freelance writer who covers health, fitness, outdoors, and travel. She holds a B.A. in journalism from the SI Newhouse School of Public Communications at Syracuse University, a personal trainer certification from the American Council on Exercise (ACE), and a CPR certification from the American Red Cross. You can find her work on SELF, TheHealthy, Health, Martha Stewart Living, HealthCentral, and more. When she's not busy writing or editing, she enjoys hiking, running, cooking, and lounging on the couch watching the latest Netflix documentary.