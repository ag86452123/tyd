---
title: Terms
id: terms
slug: terms
template: default
show_headline: true
headline: Terms of Use of TheYaddo
show_excerpt: false
show_dates: true
dates:
  date_published: 2020-05-12T14:18:41Z
  date_updated: 2020-05-13T14:18:44Z
show_cover: false
cover_image:
  description: This image has no alternative description.
  image: "../../src/assets/images/max-libertine-S0PqpcqCKaA-unsplash.jpg"
---
**Please read these Terms and Use before using, or submitting content. By continuing to use Refinery29, or by submitting content to Refinery29 you agree to abide and be bound, by these Terms of Use.**

The following are the legally binding terms and conditions (collectively with the Privacy Policy at [http://www.refinery29.com/privacy](http://www.refinery29.com/privacy "http://www.refinery29.com/privacy"), between each user (“you” or “your”) and Refinery 29 Inc. (“REFINERY29,” “we,” “us,” “our” or “ours”) and govern the use of [www.refinery29.com](http://www.refinery29.com/) and the content, features, services, social media channels and applications offered by REFINERY29 (collectively “Service”). If you do not agree to any of these terms and conditions, please do not use the Service.

#### REFINERY29’s Proprietary Rights

All materials that are included in, made available on or are otherwise a part of the Service (“REFINERY29 Content”), including without limitation any and all articles, blogs, text, photos, images, illustrations, videos, application software, technologies, source and object codes, designs, graphics, layouts, artwork, logos, trademarks and the “look and feel” of the Service, are owned or licensed by Refinery29 or its affiliates or subsidiaries. The REFINERY29 Content is protected by copyrights, trademarks, service marks, patents, trade secrets and/or other proprietary rights and laws.

#### Your License To Use REFINERY29 Content Available On Our Sites

We grant you a limited, personal, non-exclusive, non-commercial, revocable, non-sublicensable and non-transferable license to use, view and/or play the REFINERY29 Content, subject to your compliance with these Terms of Use. You may make one printed copy of each article on the Service solely for your personal use. You must not modify the paper or digital copies of any materials you have copied in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text. Our status (and that of any identified contributors) as the authors of content on our Service must always be acknowledged.

#### REFINERY29 Content Disclaimers

The REFINERY29 Content on our Service is provided for general informational purposes only. Although we make reasonable efforts to update the information on our Service, we make no representations, warranties or guarantees, whether express or implied, that the content on our Service (including both the REFINERY29 Content and User Content) is accurate, complete or up-to-date. YOU SHOULD NOT RELY ON THIS INFORMATION AS A SUBSTITUTE FOR, NOR DOES IT REPLACE, PROFESSIONAL LEGAL, FINANCIAL, TAX OR MEDICAL ADVICE. IF YOU HAVE ANY CONCERNS OR QUESTIONS ABOUT YOUR HEALTH OR THE CONTENT ON THE SERVICE, YOU SHOULD ALWAYS CONSULT WITH A PHYSICIAN OR OTHER HEALTH-CARE PROFESSIONAL. THE USE OF ANY INFORMATION PROVIDED ON THE SERVICE IS AT YOUR OWN RISK. No assurance is given that the information contained on the Services will always include the most recent findings or developments with respect to the particular material. We do not recommend or endorse any specific products, services, opinions, or other information that may appear on the Service.

By using the Service, you may be exposed to content from other users, as well as material posted, uploaded, made available, shared or transmitted to or through the Service by us, that you may find offensive or otherwise objectionable. The views expressed by other users on our Service do not represent our views or values. Under no circumstances will we be liable or responsible in any way to any third party, for the content or accuracy of any content posted by you or any other user of our Service.

#### Prohibited Activities

In connection with your use of the Services, you acknowledge and agree that you will not:

* Copy, reverse engineer, reverse assemble, otherwise attempt to discover the source code, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer or sell any REFINERY29 Content or information, software, products or services obtained through the Service;
* Violate any applicable local, state, national or international law, rule or regulation or use the Service for any purpose that is prohibited by these Terms of use;
* Manipulate, modify or edit the REFINERY29 Content or the Service or display the Services by using framing or similar navigational technology;
* Use the Services in any manner that could damage, disable, overburden or impair REFINERY29's servers or networks, or interfere with any other user's use or access to the Service
* Obtain unauthorized access to the Service, the server on which our Service is stored or any server, computer or database connected to the Service or harvest or otherwise collect information about other users without their consent

WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, COPYING OR REPRODUCING ANY SERVICES, PROGRAMS, PRODUCTS, INFORMATION OR MATERIALS PROVIDED BY REFINERY29 TO ANY OTHER SERVER OR LOCATION FOR FURTHER REPRODUCTION OR REDISTRIBUTION IS EXPRESSLY PROHIBITED.

In addition, you agree to comply with our Posting Guidelines below.

#### User Generated Content

We are not responsible for any content, images, videos, audio files, ideas, concepts, information or other materials our users publicly post, upload, make available or share while using the Service or, when users access the Service through our social media channels, information available from such users’ social media profiles (each and all of the foregoing, “User Content”). Any content you upload to our Service will be considered non-confidential and non-proprietary.

You will ensure that any User Content which you contribute to the Service will be accurate (where it states facts) and be genuinely held (where it states opinions). While we have no obligation to review, monitor, display, accept or exploit User Content, we may, in our sole discretion, delete, move, re-format, edit, alter, distort, remove or refuse to exploit User Content at any time, for any reason, without notice or liability to you or any other party.

#### Posting Guidelines

When posting information on the Service, use good taste when discussing sensitive topics. Users are required to treat others with respect and honesty. Be fair and informative. Post honest and valuable information and don't post rumors or negative opinions that are not supported by facts.

In addition to the prohibited activities described above, when posting information and media through the Services, you **must not**:

* Post anything that interferes with or disrupts the Service or the operation thereof, including files that contain malicious code, viruses, corrupted files, or any other similar software or programs that may damage the operation of another’s computer, network or the Service;
* Post statements or materials that are libelous or defame, harass, abuse, stalk, threaten, intimidate or in any way infringe on the rights of others;
* Post statements or materials that violate other contractual or fiduciary rights, duties or agreements;
* Post or upload personal information, pictures, videos or any other media of another person without their express permission;
* Post statements or materials that are bigoted, hateful, racially offensive, vulgar, obscene, pornographic, profane, or otherwise objectionable, including language or images that typically would not be considered socially or professionally responsible or appropriate in person;
* Post statements or materials that encourage criminal conduct or that would give rise to civil liability or otherwise violate any law or regulation in any jurisdiction;
* Post statements or materials that in any way harm minors;
* Post statements or materials that impersonate any other person or entity, whether actual or fictitious, including, without limitation, employees and representatives of REFINERY29;
* Post statements or materials that misrepresent your affiliation with any entity and/or REFINERY29;
* Post anything that violates the privacy or publicity rights of any other person, including, without limitation, posting any personal identifying information of another individual, including, without limitation, addresses, phone numbers, email addresses, Social Security numbers, credit card numbers or any trade secrets or information for which you have any obligation of confidentiality;
* Post statements or materials that constitute junk mail, spam or unauthorized advertising or promotional materials, including, without limitation, links to commercial products or services or any political campaigning;
* Post material that infringes, or that may infringe, any patent, trademark, trade secret, copyright or other intellectual or proprietary right of any party, or that you otherwise do not have the right to make available, without the express permission of the owner of the copyright, trademark or other proprietary right. REFINERY29 does not have any express burden of responsibility to provide any user with indications, markings or anything else that may aid any user in determining whether the material in question is copyrighted or trademarked. Users shall be solely liable for any damage resulting for infringements of copyrights, trademarks, proprietary rights or any other harm resulting from such submission.

Any user failing to comply with these guidelines may be expelled from and refused continued access to the message boards, chats or other public forums in the future. REFINERY29 or its designated agents may remove or alter any user-created content at any time for any reason. Materials posted and/or uploaded to the various public forums may be subject to size and usage limitations. You are responsible for adhering to such limitations.

You are responsible for configuring your information technology, computer programs and platform in order to access our Service. You should use your own virus protection software.

#### User Content License Grant to REFINERY29

By submitting or posting your User Content to or on the Service, you grant us, and our affiliates, agents, third party partners, and assignees a worldwide, non-exclusive, sublicensable, transferable, assignable, royalty-free and irrevocable license in perpetuity to host, display, transmit, distribute, publicly perform, reproduce, modify, edit, translate, copy, create derivative works from, store, sell, promote, sublicense and otherwise use your User Content in any and all manner and media, whether now known or hereafter devised, for any purpose whatsoever, commercial or otherwise. You also release all moral rights and similar rights in and to your User Content. You further perpetually and irrevocably grant REFINERY29 the unconditional right to use and exploit your name, persona and likeness included in any User Content and in connection with any User Content, without any obligation, remuneration or attribution of any kind. You waive all rights and release REFINERY29 from any claim for defamation, invasion of right to privacy, infringement of rights of publicity or any similar matter, based upon or relating to the use of any User Content. You represent and warrant that any person or entity named or pictured in such User Content has provided any necessary licenses, rights, waivers or authorizations to allow REFINERY29's use of such User Content in accordance with such license.

#### Sponsored Content

We may receive free products from marketers that we sometimes review or discuss in our editorial content. We may also run advertisements on our sites concerning some of those products or companies that sell them (and other products sold by such companies) for which we sometimes receive compensation.

#### Special Promotion Terms

Occasionally, we may offer the chance to participate in sweepstakes, contests, surveys and/or special offerings (“Special Promotion(s)”) through the Service. Special Promotions may be governed by terms and conditions that are separate from these Terms of Use. It is your responsibility to read those Special Promotion terms and conditions to determine the sponsor's requirements of you in connection with the applicable Special Promotion. If the provisions of a Special Promotion’s terms and conditions conflict with these Terms of Use, those separate terms and conditions will prevail with respect to such Special Promotion, unless otherwise stated.

#### Links To The Service

You may link to our website, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it. You must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists. You must not establish a link to our Service in any website that is not owned by you. Our Service must not be framed on any other Service.

We reserve the right to revoke any link to any page of the Service or our social media channels in our sole discretion without notice. We also reserve the right to require that you obtain our prior written consent before you provide any links to any page or social media page of the Service.

#### Third Party Links

We may provide, or third parties may provide, on or through the Service, links to third party sites, such as other websites or resources. These third party sites are operated by parties who are separate from REFINERY29. We have no control over such third party sites, we are not responsible for the availability of such third party sites and we do not endorse such third party sites. We are not responsible for or liable for any losses, expenses or damages, to you or your software, hardware or data, arising out of the operations of such third party sites.

The Service is a participant in affiliate marketing programs designed to provide a means for websites to earn advertising fees by advertising and linking to participating advertisers and retailers. This means that we may earn a commission if/when you click on or make purchases via affiliate links. Your communications and dealings with third parties through such links including, without limitation, the payment and delivery of products and services, and any terms, conditions, warranties and representations associated with any such communications and dealings, are solely between you and the third party. You access these links at your own risk, regardless of whether or not we receive compensation, commission or share of revenues generated by purchases via such links.

#### Service Modification, Suspension Or Termination

We do not guarantee that our Service, or any content on it, will always be available or be uninterrupted. Access to our Service is permitted on a temporary basis. We reserve the right at any time and from time to time to withdraw, modify or discontinue, temporarily or permanently, all or any part of the Service with or without notice. We will not be liable to you or to any third party for any such modification, suspension or discontinuance of the Service for any period.

We may at our sole discretion, under any circumstances, for any or no reason whatsoever and without prior notice to you, immediately terminate your access to the Service. We will not be liable or responsible to you or any third party for such termination. We also reserve the right to investigate suspected violations of these Terms of Use. Any violation of these Terms of Use may be referred to law enforcement authorities.

#### Disclaimer Of Warranties

THIS SERVICE IS AVAILABLE “AS IS” AND “AS AVAILABLE” AND “WITH ALL FAULTS.” YOU USE THE SERVICE AT YOUR SOLE RISK. TO THE FULLEST EXTENT PERMISSIBLE BY LAW, REFINERY29 AND ITS OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, LICENSORS, SUPPLIERS, PARTNERS AND AFFILIATES DISCLAIM ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, THOSE OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, ACCURACY, COMPLETENESS, TIMELINESS OR RELIABILITY WITH RESPECT TO THIS SERVICE OR ANY INFORMATION OR GOODS THAT ARE AVAILABLE OR ADVERTISED OR SOLD THROUGH THE SERVICE. WE DO NOT WARRANT THAT THIS SERVICE WILL BE SEURE OR FREE FROM BUGS OR VIRUSES OR BE UNINTERRUPTED OR ERROR-FREE. THERE MAY BE DELAYS, OMISSIONS, INTERRUPTIONS AND INACCURACIES IN THE NEWS, INFORMATION OR OTHER MATERIALS AVAILABLE THROUGH THIS SERVICE;

#### Limitation Of Liability

REFINERY29 AND ITS OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, LICENSORS, SUPPLIERS, PARTNERS AND AFFILIATES ARE NOT LIABLE TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES OF ANY KIND, INCLUDING, DAMAGES FOR LOSS OF PROFITS, REVENUES, BUSINESS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES.

#### Indemnification

To the extent permitted by applicable law, you agree to indemnify, defend and hold harmless REFINERY29, its officers, directors, employees, agents, licensors, suppliers, partners and affiliates from and against all losses, liabilities, expenses, damages, and costs, including reasonable attorneys' fees, resulting from any violation of these Terms of Use or any activity related to use of the Service (including negligent or wrongful conduct) by you or any other person accessing the Service via your email account(s), social media account(s) or ISP address. We reserve the right to take over the exclusive defense of any claim. This section will survive termination of your access to the Service.

#### Designated Agent For Copyright Infringement Claim

If you have a good faith belief that your copyright is being infringed by any content on the Service, please send a notice that includes information listed below (the “Notice of Claimed Infringement”) to our Designated Copyright Agent:

Notice of Claimed Infringement must include at a minimum the following information:

* a physical or electronic signature of the owner or a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;
* an identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works are covered by a single notification, a representative list of such works;
* identification about where the material that is claimed to be infringing is found on the Service reasonably sufficient to permit REFINERY29 to locate such material;
* information reasonably sufficient to permit REFINERY29 to contact you, such as an address, telephone number, and email address at which the you may be contacted;
* a statement that the you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent or the law; and
* a statement that the information in the notification is accurate, and under penalty of perjury, that you are the owner or authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.

If you believe that any material has been removed as a result of such mistake of misidentification, send a notice to our Designated Copyright Agent that includes the following information:

* your physical or electronic signature;
* an identification of the material that has been removed or to which access has been disabled and the location at which such material appeared before it was removed or access to it was disabled;
* a statement under penalty of perjury that you have a good faith belief that the material was removed or disabled as a result of mistake or misidentification of the material that was removed or disabled; and
* your name, address, and telephone number, and a statement that you consent to the jurisdiction of Federal District Court for the judicial district in which your address is located, or if your address is outside of the United States, for any judicial district in which REFINERY29 may be found and that you will accept service of process from the party who provided the Notice of Claimed Infringement or an agent of such party.

If a counter-notice is received by the Designated Copyright Agent, REFINERY29 may send a copy of the counter-notice to the original complaining party informing such party that REFINERY29 may replace the removed material or cease disabling it in 10 business days. Unless the original complaining party files an action seeking a court order against us or the party contesting the original complaint, the removed material may be replaced or access to it restored in ten (10) to fourteen (14) business days or more after receipt of the counter-notice, at REFINERY29’s discretion.

#### Jurisdictional Issues

Our Service is directed to people residing in the United States. We do not represent that content and materials available on or through our Service is appropriate or available in other locations. Those who choose to access the sites or use the Services from other locations do so on their own initiative and at their own risk, and are responsible for compliance with local laws, if and to the extent applicable. We may limit the availability of our Service or any service or product described on our Service to any person or geographic area at any time.

#### Governing Law; Jurisdiction; Arbitration; Commencement of Actions

These Terms of Use and the relationship between you and REFINERY29 will be governed by the laws of the United States of America and the State of New York as an agreement wholly performed therein without regard to its conflict of law provisions and the United Nations Conventions on Contracts (if applicable).

Any dispute relating in any way to your use of the Service will be submitted to confidential arbitration in New York, New York, except that, to the extent you have in any manner violated or threatened to violate our intellectual property rights, we may seek injunctive or other appropriate relief in any state or Federal court in the state of New York, and you consent to exclusive jurisdiction and venue in such courts. Arbitration under these Terms of Use will be conducted under the rules then prevailing of JAMS. The arbitrator’s award will be binding and may be entered as a judgment in any court of competent jurisdiction. To the fullest extent permitted by applicable law, no arbitration under these Terms of Use will be joined to an arbitration involving any other party subject to these Terms of Use, whether through class arbitration proceedings or otherwise.

Any claim or cause of action arising out of or related to use of the Service or these Terms of Use must be filed within one year after such claim or cause of action arose or be forever barred. Any claim by you that may arise in connection with these Terms of Use will be compensable by monetary damages and you will in no event be entitled to injunctive or other equitable relief. You also hereby expressly waive any right to resort to any form of class action.

#### Miscellaneous

These Terms of Use constitute the entire agreement between you and us and govern your use of the Service. These Terms of Use supersede and extinguishes any prior agreements, promises, assurances, warranties, representations and undertakings between you and us, whether written or oral with respect to the Service. You also may be subject to additional terms and conditions that may apply when you use other services, affiliate services, or third-party services.

These Terms of Use are fully assignable by us and will be binding upon and inure to the benefit of our successors and assigns. No agency, partnership, joint venture, employee-employer or franchiser-franchisee relationship is intended or created by these Terms of Use. Our failure to exercise or enforce any right or provision of these Terms of Use will not constitute a waiver of such right or provision. If any provision of these Terms of Use is found by a court of competent jurisdiction to be invalid, the court should nevertheless endeavor to give effect to the parties’ intentions as reflected in the provision, and the other provisions of these Terms of Use will remain in full force and effect. The headings in these Terms of Use are for convenience only and have no legal or contractual effect.

We shall not be liable for any failure to perform our obligations hereunder where such failure results from any cause beyond our reasonable control, including, without limitation, mechanical, electronic or communications failure or degradation.

#### Updates To These Terms

We reserve the right to change any terms of these Terms of Use at any time without prior notice to you. You agree that our posting of revised Terms of Use on our Service will constitute notice to you of such revised Terms of Use and that your access or use of the Service after such posting constitutes your agreement to be bound by such changes and the revised Terms of Use. Consequently, please review these Terms of Use before each and any access or use of our Service. Any changes to these Terms of Use will become a part of these Terms of Use and will apply as soon as they are posted or such later date as may be specified in the posted Terms of Use.

Unless explicitly stated otherwise, any new features or functionality that augment or enhance the Service will be subject to these Terms of Use.