---
title: A Default Page
headline: Big Bold Default Headline
dates:
  date_published: 2020-05-14T11:15:04.000+00:00
  date_updated: 2020-05-14T11:15:04.000+00:00
cover_image:
  description: This image has no alternative description.
  image: "../../src/assets/images/max-libertine-S0PqpcqCKaA-unsplash.jpg"
show_headline: true
show_dates: true
show_cover: true
slug: default
template: default
excerpt: In the absence of an excerpt, a nice short description of this page, here
  I am as a default one. Except, I'm fewer than 155 characters, or am I?
show_excerpt: true

---
We’re a fully distributed team of 85 people living and working in 15 countries around the world. And we’re working to build the best products to help our customers build their brands and grow their businesses on social media.

We’ve always aimed to do things a little differently at Buffer. Since the early days, we’ve had a focus on building one of the most unique and fulfilling workplaces by rethinking a lot of traditional practices.

A commitment to supporting our team and our customers has helped Buffer grow from humble beginnings to now serving more than 73,000 customers. Our passion for making meaningful connections flows through everything we do.

![](../../src/assets/images/the_yaddo_default_image.jpg)

We care about building a quality product, trusted relationships with our customers, and a sense of community that connects our customers and team with one another.

To learn more about our approach to business and work, feel free to hop on over to our Open Blog.

We’ve always aimed to do things a little differently at Buffer. Since the early days, we’ve had a focus on building one of the most unique and fulfilling workplaces by rethinking a lot of traditional practices.

> To learn more about our approach to business and work, feel free to hop on over to our Open Blog.

A commitment to supporting our team and our customers has helped Buffer grow from humble beginnings to now serving more than 73,000 customers. Our passion for making meaningful connections flows through everything we do.