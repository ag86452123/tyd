---
title: About
id: about
slug: about
template: about
excerpt: TheYaddo is where women find freedom in self. Read, listen, and watch evergreen
  content for body positivity, sexual liberation, strength and growth.
grid_hero:
  headline: Find freedom in self.
  link_button:
    text: Latest Story
    link: "/"
  top_leading_image: "../../src/assets/images/ronise-da-luz-5DJ8sR-m58I-unsplash.jpg"
  top_leading_image_description: Lorem ipsum is placeholder text commonly used in
    the graphic, print
  top_trailing_image: "../../src/assets/images/max-libertine-S0PqpcqCKaA-unsplash.jpg"
  top_trailing_image_description: Lorem ipsum is placeholder text commonly used in
    the graphic, print
  bottom_leading_image: "../../src/assets/images/dom-hill-8xcmxckL0O0-unsplash.jpg"
  bottom_leading_image_description: Lorem ipsum is placeholder text commonly used
    in the graphic, print
  bottom_trailing_image: "../../src/assets/images/000064760008.jpg"
  bottom_trailing_image_description: Lorem ipsum is placeholder text commonly used
    in the graphic, print
cover_image:
  image: "../../src/assets/images/GettyImages-1162928074.jpg"
  description: This should be a descriptive text of the image and/or its content(s).
    This text helps to make the image accessible to people who cannot see, for example.

---
## Who We Are

TheYaddo is a free-thinking community that compels women to find freedom in the self. Through a constant stream of evergreen content to read, listen, and watch, women are helped to find confidence and beauty in everything around them using the beauty from within them.

> Our mission is to help women find freedom by arming them with information and products that promote body positivity, sexual liberation, strength and growth.

Listed on Crain's Fast 50 for three consecutive years, as well as one of Fast Company’s Most Innovative Media Companies, the brand has received a variety of acknowledgements, including being named one of the fastest growing media companies in America by Inc. and the European Publisher of the Year by Digiday.

As the original next-generation women’s media company, we've changed the way content and storytelling speaks to and represents women, always sparking the conversations that matter most.

Delivering on that mission, we've driven intention-fueled initiatives such as [Shatterbox](https://www.refinery29.com/en-us/shatterbox), a short-film series dedicated to increasing opportunities for women in film; [The 67%](http://www.refinery29.com/67-percent-project-plus-size-body-image/), dedicated to more and better plus-size representation and equality; [29Rooms](https://www.29rooms.com/), our festival of cause, culture and creativity; [UnStyled](https://podcasts.apple.com/us/podcast/unstyled/id1171140955), a top-rated style and culture podcast that spotlights trailblazing women; and [Unbothered](https://www.instagram.com/r29unbothered/), a channel created for, by and about black women.

We celebrate imperfections, banish taboos, and always strive to be real and relatable — walking the walk in all that we do.

TheYaddo!