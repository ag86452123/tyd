---
title: Privacy
id: privacy
slug: privacy
template: default
show_headline: true
headline: Your Privacy Matters to Us
show_dates: true
dates:
  date_published: 2020-05-13T14:18:01Z
  date_updated: 2020-05-14T14:18:05Z
show_cover: false
cover_image:
  description: This image has no alternative description.
  image: "../../src/assets/images/max-libertine-S0PqpcqCKaA-unsplash.jpg"
---
This privacy policy describes how we treat and use the information we collect when you visit or use our websites Refinery29.com, mobile-optimized version of the website, or software applications to which this policy is linked (collectively, “Website”), including when you sign-up for our newsletters. Refinery 29 Inc., located at 225 Broadway, New York, NY 10007, U.S.A. manages the U.S.A. sections of our Website.

This Website is operated in the United States and in accordance with the laws of the United States.

We provide you this privacy policy to help you understand the kinds of information we may gather about you when you visit the Website or use any of its services, how we may use and disclose the information, and how you can control, correct and/or update the information.

To be clear about the terminology we are using, when we use the phrase "personal information" in this policy, we mean information that specifically identifies you as an individual, such as a person’s full name, address, e-mail address, date of birth or phone number.

When we use the phrase “anonymous” information/form in this policy, we mean information that does not specifically identify an individual person, such as a user’s areas of interest, browsing history, demographic information or a unique identifier (such as a cookie or IP address).

By visiting the Website or using the services, you are accepting the policies and practices described in this policy. Each time you visit the Website or use the services, you agree and expressly consent to our collection, use and disclosure of the information that you provide as described in this policy.

### Collection of Information

**Information You Provide.** We collect personal information that you voluntarily provide. For instance, contact information, and other personal details may be collected when you: sign up for our newsletters, complete a survey, post a comment on our Website, enter a sweepstakes or contest, or participate in our social networking features. Information you may be asked to provide may include your email address, your name, buying preferences, annual income, other demographic or profile information, Twitter, Facebook and other social media network usernames and aliases and other information you choose to submit.

Additionally, if you elect to post material to any blogs, forums, or participate in our community boards that may be offered on our Website, then such materials will be collected and any materials you choose to make publicly available, including your personal information, may be available for all others to view. Even after you terminate your account, all such materials submitted by you on the Website or our social media channels may be retained by us and we may continue to disclose such materials publicly. By using these interactive forums, you agree that we are not responsible for any information that you disclose or communicate in such forums, and any disclosure you make are at your own risk.

If you purchase products or services through our Website storefront(s), Refinery29 and third-party merchants, as well as third-party credit card processors may access and collect your personal information, including, but not limited to, your full name, address, email address, phone numbers(s), and credit card details.

**Location Information and IP Address.** We and our service providers and advertisers may collect and share your location for personalization and marketing purposes using certain digital applications, wireless access protocol services ("WAP Services") or mobile phone services. We, or the service provider, will obtain your prior consent to such collection to the extent required by applicable law. An "Internet protocol address" or "IP Address" is a number that is automatically assigned to your computer when you use the Internet. We, or our service providers, may use your IP Address when you access the Website or use other means to assist with delivering geographically targeted advertisements. For more information, please see the “Cookies and Targeted Advertising” section below.

**General Usage Information.** We may gather usage information about Website users on an anonymous basis. Examples of such information include: amount of time spent on our site, pages viewed, websites users originated from and websites users visit after leaving our Website. This information helps us create a faster, efficient and more relevant website. We may also share aggregate and anonymous usage information with our prospective and actual business partners, advertisers and other third parties for any business purpose.

Refinery29.com features Nielsen’s proprietary measurement software which will allow you to contribute to market research, like Nielsen’s TV Ratings. Please see[ www.nielsen.com/digitalprivacy](http://www.nielsen.com/digitalprivacy) for more information or to opt-out of contributing to such research.

### Cookies and Targeted Advertising

We may use small pieces of information placed on your browser, known as “cookies”, to provide you with a more personal and interactive experience. We may use cookies to determine what advertisements you see, control the sequence of advertisements and make sure you don't see the same advertisement too many times. For contests or sweepstakes, we may use cookies in order to track your progress and the number of entries in some promotions. For polls or surveys, we may use cookies to help ensure that an individual can't vote more than once on a particular question or issue. We may use cookies along with pixels, web beacons (which are usually small, transparent graphic images), operating system and device information and navigational data like Uniform Resource Locators (URL), to gather information regarding the date and time of your visit, the features and information for which you searched and viewed, the email you opened, or on which advertisements you clicked. Companies Refinery29 works with may also use cookies for understanding Web usage patterns of users who see advertisements on the Website. We do not store personal information in any cookies on your computer.

Cookies can be removed by following your Internet browser's directions usually within the help menu. In order to use certain features of the Website, your web browser must accept cookies. If you choose to disable cookies, some aspects of the Website may not work properly, and you may not be able to access the Website.

We believe that advertising is more interesting to you when it is relevant. Accordingly, we customize the advertisements that you see based upon: (i) geographic location information, which we may determine through your IP address or other ways; (ii) data we receive from third parties and/or; (iii) your visits on the Website or use of other services (commonly referred to as behavioral advertising or interest-based advertising). We may use your personal information to deliver customized advertising to you on this Website, on other websites and on other digital and offline media channels. We may use third-party vendors to connect information collected on the Website (such as IP addresses, geolocation, cookie-related data, and other information described above) with information collected from other websites, devices and media channels and then show you advertising based on this combined information. The advertisement may appear when you are visiting a different section of the Website or another website, device or media channel.

If you would like more information about behavioral advertising and to know your choices with respect to it, please visit [www.aboutads.info/choices](http://www.aboutads.info/choices). Many vendors that engage in behavioral advertising participate in the Network Advertising Initiative (NAI) and/or the Digital Advertising Alliance (DAA) and abide by NAI and/or DAA principles. If you wish to opt out of participating in advertising programs conducted by NAI and DAA members or learn more about the NAI and DAA, please go to the [NAI Opt-Out website](http://optout.networkadvertising.org/) or [DAA Ad Choice Page](http://youradchoices.com/) and follow the instructions. After you opt-out, you will still see advertisements, but the advertising may not be as relevant to your interests. If you change your computer, change your browser or delete your cookies, you will need to renew your opt-out. For opt-out options specific to Google Analytics, please visit[ tools.google.com/dlpage/gaoptout](https://tools.google.com/dlpage/gaoptout). If you delete your cookies or upgrade your browser after having opted out, you will need to opt out again. If you use multiple browsers or devices, you will need to execute this opt-out on each browser or device.

Advertisers or other third parties on the Website may also engage in behavioral advertising and use cookies and web beacons in the manner described above. We do not control these advertisers or other parties' use of cookies or web beacons or what they do with the information they collect.

We may use Facebook to deliver ads on the Website. Facebook may use information you share on Facebook and information gathered from your use of other websites and apps to delivery ads to you. To learn more about this type of advertising and your choices about it, visit [www.facebook.com/about/ads.](https://www.facebook.com/about/ads)

Our Website is currently not setup to respond or process web browser “Do Not Track” (“DNT”) signals We do adhere to the Self-Regulatory Principles for Online Behavioral Advertising of the Digital Advertising Alliance (for more information click here).

**Information We Receive From Third Parties.** We may combine online and/or offline information received from third parties with the information we have already collected from you via our Website or services. The third party information is used for a variety of purposes including to verify other information about you (e.g., verify your mailing address to send you requested products or services), to conduct research, and to enhance the content and advertising we provide to you.

### Use of Information

We use your personal information:

* to send you promotional/marketing information, newsletters, offers or other information regarding opportunities and functionality that we think would be of particular interest to you;
* to fulfill any other purpose for which you provide the information; for example, if you enter contests or sweepstakes, we may use your email to notify that you won or if you purchase products through our Website storefront, to process payment (including credit card information) and shipping details;
* to notify you, at our discretion, about changes to our Website or any products or services we offer or provide though it;
* to allow you to participate in interactive features on our Website;
* to analyze, benchmark and conduct research of user data and user interaction with the Website and to gain insights that help us to make improvements to the design and performance of the Website;
* in any other way we may describe when you provide the information; and
* for any other purpose with your consent or as permitted by law.

We may disclose and release your personal information (including without limitation content or other data posted, uploaded or provided by you) to third parties:

* to enable third party service providers to provide products, services or functions on our behalf or to help us to operate our business and the Website. Such service providers may include vendors and suppliers that provide us with technology, research, data analytics, ad serving, email services, payment processing, customer service, and/or marketing assistance.
* to comply with valid requirements such as a law, regulation, search warrant, subpoena or court order;
* in special cases, such as a physical threat to you or others, a threat to homeland security, a threat to our system or network, or in any cases in which we believe it is reasonably necessary to investigate or prevent harm, fraud, abuse, illegal conduct or violation or alleged violation of this privacy policy or other agreement we may have with you;
* to enforce the Terms of Use;
* with your consent or in accordance with your indicated preferences (e.g., you indicated to receive materials directly from a third party partner);
* to respond to your requests for customer service; and
* in connection with a sale, merger, transfer, exchange, or other disposition (whether of assets, stock, or otherwise) of all or a portion of the business conducted by the Website to which this policy applies, in which case the acquiring company will possess the information collected by us.

We may also disclose your information to our subsidiary, affiliate or parent company.

If we partner with a retailer or other third party to offer online shopping opportunities, promotions, contests or sweepstakes, polls, surveys, services, subscriptions and other applications, then personally identifiable information you provide in connection with the above transactions may be collected directly by or shared by us with the third party. These third parties will use your information in accordance with their own privacy policy. In some cases, such as where a provider’s service is embedded in our Website (such as an embedded video or social network tool), you must contact the third party to learn about your objection options, and the third party (not us) will be responsible for addressing your objection. Refinery29 is not responsible for their privacy policy or practices.

We may share anonymous information about you with third parties for any purpose. Personal information that has been aggregated with the personal information of other users in a way that does not identify you as an individual shall be treated as anonymous information. Currently we are using Google Analytics to analyze the audience of the website and improve our content. No personal information is collected from Google Analytics. For further information on the privacy policy concerning Google Analytics, please go here.

If you do not want us to use or disclose personal information collected about you in the ways identified if this privacy policy, you should not use the Website or services.

### Social Networks and Social Media Integration

If you use your login credentials from a social networking site (e.g., Facebook, Twitter, Instagram, Pinterest) ("SNS") on our Website, we may receive information from such SNS in accordance with the terms and conditions (e.g., terms of use and privacy policy) of the SNS ("SNS Terms"). If you elect to share your information with these SNS, we will share information with them. The SNS Terms of these SNS will apply to the information we disclose to them. Additionally, if you make an election with respect to a SNS while on our Website (for instance, if you elect to “follow” us on one of the SNS, or “Like” us on Facebook), your election will be reflected and may be shared with your social network within the applicable SNS. We reserve the right to use and disclose all personally identifiable information and other information about you that we receive through SNS in the same ways described in this privacy policy.

You are encouraged to read all policies and information on the applicable social media services to learn more about how they handle your information before using any such features made available to you through our Website. We are not responsible for any acts or omissions by any social media service provider or your use of features on their platforms.

### How to Opt Out

You can unsubscribe from receiving Refinery29 newsletters or commercial emails at any time. You can unsubscribe by clicking on the “Unsubscribe” link in the footer of any Refinery29 email and following the instructions. But note that you may continue to receive certain communications from us, such as transactional or relationship messages, and/or messages about your account/profile. In order to cancel your account/ profile, please email [support@refinery29.com](mailto:support@refinery29.com).

### Links to Other Sites

As a curator of information, Refinery29 often links out to other sites. We are not responsible for any content that appears on these sites, nor do we endorse them. Third parties may display advertisements on the Service and on such web sites. These advertisements may contain cookies, web beacons and other similar technologies. These third parties may collect information or insert cookies or similar technologies without our knowledge. For questions about these sites, please consult their individual privacy policies. Refinery29 is not responsible for the privacy practices employed by any third party.

### Co-Branded Sections of the Website

Certain portions of the Website may be co-branded with a business partner or subject to a different privacy policy that is either the partner's privacy policy or a privacy policy developed jointly by Refinery29 and our partner (collectively the "Co-branded Policy"). The partner's collection, use and dissemination practices regarding any data or personal information provided by, or obtained from users of these Co-branded portions of the Website will be governed by the Co-branded Policy. Please read the Co-branded Policy for each Co-branded Website before providing any personal information.

### Security and Retention

We store the information collected through the Website on restricted database servers. We use a variety of industry-standard security technologies and procedures to help protect your information from unauthorized access, use, or disclosure. Although we take appropriate measures to safeguard against unauthorized disclosures of your information, we cannot guarantee its absolute security.

### Personal Information Processed in the USA and Elsewhere

Please note that the personal information you provide to us will be processed in the United States of America or other countries where laws regarding processing of personally identifiable information may be less stringent than the laws in your country. We will take steps reasonably necessary to ensure that your data is treated and secured in accordance with this privacy policy.

**Children.** We do not offer the Website to anyone under the age of majority in their jurisdiction of residence and do not knowingly collect personal information from children under 13. No one under age 13 may provide any information to or on the Website. If you are under 13, do not use or provide any personal information on or through the Website, register on the Website, or use any of the interactive or public comment features of this Website or provide any information about yourself to us, including your name, email address or any screen name or user name you may use.

If you are the parent or guardian of a child whom you believe has disclosed personal information to us, please contact us at [legal@refinery29.com](mailto:legal@refinery29.com) so that we may delete and remove such child’s information from our systems. In addition, in the event that a minor under the age of 18 in California uses the Website, the minor may request that Refinery29 remove content or information posted by the minor on the Website by sending an email to[ privacy@refinery29.com](mailto:privacy@refinery29.com). However, such removal may not remove all traces of such posting everywhere online, since content or information may have been shared by others. Refinery29 may not have to comply with this removal requirement if: (i) federal or state law requires that we maintain the content or information; (ii) the content was stored or posted (or reposted) by a third party other than the minor contacting Refinery29; (iii) we anonymize the content or information so that the minor cannot be identified; (iv) the minor received compensation or other consideration for providing the content; or (v) the minor does not follow the instructions in this section.

### Your California Privacy Rights: Notice to California Customers

Subject to certain limits under California Civil Code § 1798.83, if you are a California resident, you may ask us to provide you with (i) a list of certain categories of personal information that we disclosed to third parties for their direct marketing purposes during the immediately preceding calendar year and (ii) the identity of third parties that received personal information from us for their direct marketing purposes during that calendar year. The list will not be specific to disclosures your particular personal information. To make such request, send us an email at[ privacy@refinery29.com](mailto:privacy@refinery29.com) or write us at Refinery 29 Inc. 225 Broadway, 23rd Floor, New York, NY 10007, indicating “Your California Privacy Rights” in the subject of your email or letter, your name and complete mailing address. We are not responsible for requests that are not labeled or sent properly, or do not have complete information. Please note that we are only required to respond to one request per customer each year.

### Jurisdictional Issues

Refinery29 makes no representation or warranty that the content and materials on the Website are appropriate or available for use in locations outside of the United States. Those who choose to access the Website or use the services from other locations do so on their own initiative and at their own risk, and are responsible for compliance with local laws, if and to the extent applicable.

### Modifications

Refinery29 may make changes to this privacy policy at any time. If and when we do, we will inform you by posting the revised privacy policy on this Website. You are responsible for periodically visiting our Website and this privacy policy to check for any changes. Any changes that are made to this privacy policy will apply to both any personal information that we hold prior to the effective date of the amended privacy policy and any personal information that we collect on or after such effective date. Your continued use of the Website after this privacy policy has been amended shall be deemed to be your continued acceptance of the terms and conditions of the privacy policy, as amended.

### Contact Us

If you have questions about this privacy policy, or about the Website, please contact us at Refinery 29 Inc., Attn. Legal Department, 225 Broadway, 23rd Floor, New York, NY 10007,[ legal@refinery29.com](mailto:legal@refinery29.com).