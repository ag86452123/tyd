import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        colors: {
            primary: '#ff0f7b',
            aliceblue: '#cfe7f7',
            milkthistle: '#ffe1e1',
            ivory: '#fff3dc',
            floral: '#ffffdc',
            blush: '#ffdaff'
        },
        drawer: false,
    },
    getters: {
        applyColors: state => state.colors,
        drawer: state => state.drawer,
        primary: state => state.colors.primary
    },
    mutations: {
        openDrawer: state => state.drawer = true,
        closeDrawer: state => state.drawer = false,
    }
})